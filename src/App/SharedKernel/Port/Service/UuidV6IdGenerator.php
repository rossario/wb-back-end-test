<?php

namespace App\SharedKernel\Port\Service;

use Ramsey\Uuid\Uuid;

readonly class UuidV6IdGenerator implements IdGenerator
{
    public function get(): string
    {
        return Uuid::uuid4()->toString();
    }
}