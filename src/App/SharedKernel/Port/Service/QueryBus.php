<?php

declare(strict_types=1);

namespace App\SharedKernel\Port\Service;

interface QueryBus
{
    public function get(object $query): mixed;
}