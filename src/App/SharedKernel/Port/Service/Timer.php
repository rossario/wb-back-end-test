<?php

declare(strict_types=1);

namespace App\SharedKernel\Port\Service;

use DateTimeImmutable;

class Timer
{
    public function __construct(
        private int $relativeTimestamp = 0
    ) {}

    public function setRelativeTimestamp(int $relativeTimestamp): void
    {
        $this->relativeTimestamp = $relativeTimestamp;
    }

    public function nowTimestamp(): int
    {
        return time() + $this->relativeTimestamp;
    }

    public function nowImmutable(): DateTimeImmutable
    {
        return DateTimeImmutable::createFromFormat('U', (string) $this->nowTimestamp());
    }
}