<?php

declare(strict_types=1);

namespace App\SharedKernel\Port\Service;

interface DeliveryBus extends CommandBus
{
    public function send($message): null;
}