<?php

declare(strict_types=1);

namespace App\SharedKernel\Port\Service;

interface CommandBus
{
    public function send(object $message): mixed;
}