<?php

namespace App\SharedKernel\Port\Service;

interface Transformer
{
    public function transform(mixed $from, object|string $to = null, array $groups = []): mixed;
}