<?php

declare(strict_types=1);

namespace App\SharedKernel\Component\Report\Event;

final readonly class ReportGenerated
{
    public function __construct(
        public string $reportId
    ) {}
}