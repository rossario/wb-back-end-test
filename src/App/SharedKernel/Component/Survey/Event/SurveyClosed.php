<?php

declare(strict_types=1);

namespace App\SharedKernel\Component\Survey\Event;

final readonly class SurveyClosed
{
    public function __construct(
        public string $surveyId
    ) {}
}
