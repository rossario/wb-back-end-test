<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Enum;

enum SurveyStatusEnum: string
{
    case Draft   = 'new';
    case Live    = 'live';
    case Closed  = 'closed';

    public function isAllowedToChange(SurveyStatusEnum $newStatus): bool
    {
        return match($newStatus) {
            self::Draft => false,
            self::Live => $this === self::Draft,
            self::Closed => $this === self::Live,
        };
    }

    public static function validValues(): array
    {
        return array_column(self::cases(), 'value');
    }
}