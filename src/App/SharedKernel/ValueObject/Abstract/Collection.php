<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Abstract;

use App\SharedKernel\ValueObject\Interface\ReadableValue;
use LogicException;

abstract readonly class Collection implements ReadableValue
{
    public function __construct(
        private array $value,
    )
    {
        class_exists(static::getItemType())
        || interface_exists(static::getItemType())
        || throw new LogicException('InstanceType is not classFQN ' . static::getItemType());
    }

    abstract public function getItemType(): string;

    public function getValue(): array
    {
        return $this->value;
    }
}