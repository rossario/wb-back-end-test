<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Abstract;

use App\SharedKernel\ValueObject\Interface\ReadableValue;
use DateTimeImmutable;
use Override;

readonly class DateValue implements ReadableValue
{
    public function __construct(
        private DateTimeImmutable $value,
    ) {}

    #[Override]
    public function getValue(): DateTimeImmutable
    {
        return $this->value;
    }
}