<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Abstract;

use App\SharedKernel\ValueObject\Exception\InvalidArgumentException;
use App\SharedKernel\ValueObject\Interface\ReadableValue;
use ArrayIterator;
use Override;

readonly class TextList implements ReadableValue
{
    /** @var ArrayIterator<string>  */
    private ArrayIterator $collection;

    public function __construct(
        iterable $values,
    ) {
        foreach ($values as $value) {
            is_string($value) || throw new InvalidArgumentException('Invalid value type');
        }

        $this->collection = new ArrayIterator($values);
    }

    #[Override]
    public function getValue(): ArrayIterator
    {
        return new ArrayIterator($this->collection);
    }
}