<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Abstract;

use App\SharedKernel\ValueObject\Exception\InvalidArgumentException;
use App\SharedKernel\ValueObject\Interface\ReadableValue;
use Override;

readonly class Counter implements ReadableValue
{
    public function __construct(
        private int $value,
    )
    {
        $this->value >= 0 || throw new InvalidArgumentException('Value must be greater than or equal to 0');
    }

    #[Override]
    public function getValue(): int
    {
        return $this->value;
    }
}