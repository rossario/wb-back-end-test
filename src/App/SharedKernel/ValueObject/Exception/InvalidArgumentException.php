<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
}