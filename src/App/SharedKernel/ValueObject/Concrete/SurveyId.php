<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Concrete;

use App\SharedKernel\ValueObject\Exception\InvalidArgumentException;
use App\SharedKernel\ValueObject\Interface\ReadableValue;
use Ramsey\Uuid\Rfc4122\UuidV6;

final readonly class SurveyId implements ReadableValue
{
    public function __construct(
        private string $value,
    )
    {
        UuidV6::isValid($this->value) || throw new InvalidArgumentException('Id must be valid UuidV6 string');
    }

    public function getValue(): string
    {
        return $this->value;
    }
}