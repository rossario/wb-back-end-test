<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Concrete;

use App\SharedKernel\ValueObject\Interface\ReadableValue;
use InvalidArgumentException;
use Override;

final readonly class Email implements ReadableValue
{
    public function __construct(
        private string $value,
    )
    {
        strlen($value) > 2   || throw new InvalidArgumentException('Value must be longer than 2 characters');
        strlen($value) < 256 || throw new InvalidArgumentException('Value must be shorter than or equal to 255 characters');
    }

    #[Override]
    public function getValue(): string
    {
        return $this->value;
    }
}