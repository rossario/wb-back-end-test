<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Concrete;

use App\SharedKernel\ValueObject\Exception\InvalidArgumentException;
use App\SharedKernel\ValueObject\Interface\ReadableValue;
use Override;

final readonly class QualityScore implements ReadableValue
{
    public function __construct(
        public int $value,
    )
    {
        $this->value >= -2 || throw new InvalidArgumentException('Answer quality value must be greater than or equal to -2');
        $this->value <= 2  || throw new InvalidArgumentException('Answer quality value must be less than or equal to 2');
    }

    #[Override]
    public function getValue(): int
    {
        return $this->value;
    }
}