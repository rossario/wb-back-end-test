<?php

declare(strict_types=1);

namespace App\SharedKernel\ValueObject\Interface;

use ArrayIterator;
use BackedEnum;
use DateTimeInterface;

interface ReadableValue
{
    public function getValue(): null|bool|int|string|array|BackedEnum|ArrayIterator|DateTimeInterface;
}