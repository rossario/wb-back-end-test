<?php

declare(strict_types=1);

namespace App\SharedKernel\DTO\Report;

use App\Port\Primary\Report\Response\Report;

final readonly class ReportDTO implements Report
{
    public function __construct(
        public string $id,
        public ReportSurveyDTO $survey,
        public int $quality,
        public array $comments,
        public int $numberOfAnswers,
    )
    {}

    public function getId(): string
    {
        return $this->id;
    }

    public function getSurvey(): ReportSurveyDTO
    {
        return $this->survey;
    }

    public function getQuality(): int
    {
        return $this->quality;
    }

    public function getComments(): array
    {
        return $this->comments;
    }

    public function getNumberOfAnswers(): int
    {
        return $this->numberOfAnswers;
    }
}