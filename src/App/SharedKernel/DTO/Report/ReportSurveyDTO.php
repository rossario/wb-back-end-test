<?php

declare(strict_types=1);

namespace App\SharedKernel\DTO\Report;

use App\Port\Primary\Report\Response\ReportSurvey;

final readonly class ReportSurveyDTO implements ReportSurvey
{
    public function __construct(
        public string $id,
        public string $name,
        public string $reportEmail,
    )
    {}

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getReportEmail(): string
    {
        return $this->reportEmail;
    }
}