<?php

declare(strict_types=1);

namespace App\SharedKernel\DTO\Survey;

use App\Port\Primary\Survey\Response\SurveyAnswer;

final readonly class SurveyAnswerDTO implements SurveyAnswer
{
    public function __construct(
        public string $id,
        public string $quality,
        public ?string $comments,
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getQuality(): string
    {
        return $this->quality;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }
}