<?php

declare(strict_types=1);

namespace App\SharedKernel\DTO\Survey;

use App\Port\Primary\Survey\Response\Survey;

final readonly class SurveyDTO implements Survey
{
    public function __construct(
        public string $id,
        public string $name,
        public string $status,
        public string $reportEmail,

        /** @var array<SurveyAnswerDTO> $answers */
        public array $answers = [],
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getReportEmail(): string
    {
        return $this->reportEmail;
    }

    public function getAnswers(): array
    {
        return $this->answers;
    }
}