<?php

declare(strict_types=1);

namespace App\Contract\Report\Query;

use App\Port\Primary\Report\Query\ReportGet;
use App\Port\Primary\Report\Response\Report;

interface ReportSendEmailHandlerInterface
{
    public function handle(ReportGet $query): Report;
}