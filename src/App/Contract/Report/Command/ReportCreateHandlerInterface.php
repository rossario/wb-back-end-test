<?php

declare(strict_types=1);

namespace App\Contract\Report\Command;

use App\Port\Primary\Report\Command\ReportCreate;

interface ReportCreateHandlerInterface
{
    public function handle(ReportCreate $command): void;
}