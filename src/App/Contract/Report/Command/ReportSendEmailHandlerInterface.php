<?php

declare(strict_types=1);

namespace App\Contract\Report\Command;

use App\Port\Primary\Report\Command\ReportSendEmail;

interface ReportSendEmailHandlerInterface
{
    public function handle(ReportSendEmail $command): void;
}