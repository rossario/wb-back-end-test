<?php

declare(strict_types=1);

namespace App\Contract\Survey\Query;

use App\Port\Primary\Survey\Query\SurveyGet;
use App\Port\Primary\Survey\Response\Survey;

interface SurveyGetQueryHandlerInterface
{
    public function __invoke(SurveyGet $query): Survey;
}