<?php

declare(strict_types=1);

namespace App\Contract\Survey\Query;

use App\Port\Primary\Survey\Query\SurveySearch;
use App\Port\Primary\Survey\Response\SurveyList as SurveyListResponse;

interface SurveyListQueryHandlerInterface
{
    public function __invoke(SurveySearch $query): SurveyListResponse;
}