<?php

declare(strict_types=1);

namespace App\Contract\Survey\Command;

use App\Port\Primary\Survey\Command\AnswerCreate;

interface AnswerCreateHandlerInterface
{
    public function __invoke(AnswerCreate $command): void;
}