<?php

declare(strict_types=1);

namespace App\Contract\Survey\Command;

use App\Port\Primary\Survey\Command\SurveyRemove;

interface SurveyRemoveHandler
{
    public function __invoke(SurveyRemove $command): string;
}