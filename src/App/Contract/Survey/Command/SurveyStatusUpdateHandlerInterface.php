<?php

declare(strict_types=1);

namespace App\Contract\Survey\Command;

use App\Port\Primary\Survey\Command\SurveyStatusUpdate;

interface SurveyStatusUpdateHandlerInterface
{
    public function __invoke(SurveyStatusUpdate $command): void;
}