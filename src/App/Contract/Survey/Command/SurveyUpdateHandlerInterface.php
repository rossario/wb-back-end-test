<?php

declare(strict_types=1);

namespace App\Contract\Survey\Command;

use App\Port\Primary\Survey\Command\SurveyUpdate;

interface SurveyUpdateHandlerInterface
{
    public function __invoke(SurveyUpdate $command): void;
}