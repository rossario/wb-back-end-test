<?php

declare(strict_types=1);

namespace App\Contract\Survey\Command;

use App\Port\Primary\Survey\Command\SurveyCreate;

interface SurveyCreateHandlerInterface
{
    public function __invoke(SurveyCreate $command): string;
}