<?php

namespace App\Infrastructure\Persistence\Repository;

use App\Components\Report\Domain\Entity\Report;
use App\Infrastructure\Persistence\Doctrine\Entity\Report as ReportEntity;
use App\Infrastructure\Persistence\Doctrine\Repository\ReportEntityRepository;
use App\Infrastructure\Persistence\Service\Transformer;
use App\Port\Secondary\Persistance\Repository\Report\ReportRepository as ReportRepositoryInterface;
use LogicException;
use Override;

class ReportRepository implements ReportRepositoryInterface
{
    public function __construct(
        private ReportEntityRepository $entityRepository,
        private Transformer $transformer,
    ) {
    }

    #[Override]
    public function create(Report $report): void
    {
        $entity = $this->transformer->transform($report, ReportEntity::class);

        $this->entityRepository->save($entity, true);
    }


    #[Override]
    public function find(string $reportId): Report
    {
        $entity = $this->entityRepository->find($reportId) ?? throw new LogicException();

        return $this->transformer->transform($entity, Report::class);
    }
}
