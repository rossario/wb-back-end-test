<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Repository;

use App\Components\Survey\Domain\Entity\Survey;
use App\Infrastructure\Persistence\Doctrine\Entity\Survey as SurveyEntity;
use App\Infrastructure\Persistence\Doctrine\Repository\SurveyEntityRepository;
use App\Infrastructure\Persistence\Service\Transformer;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository as SurveyRepositoryInterface;
use LogicException;
use Override;

class SurveyRepository implements SurveyRepositoryInterface
{
    public function __construct(
        private SurveyEntityRepository $entityRepository,
        private Transformer $transformer,
    )
    {}

    #[Override]
    public function create(Survey $survey): void
    {
        $entity = $this->transformer->transform($survey, SurveyEntity::class);

        $this->entityRepository->save($entity, true);
    }

    #[Override]
    public function update(Survey $survey): void
    {
        $entity = $this->entityRepository->find($survey->getId()->getValue()) ?? throw new LogicException('Unable to update survey, not found ' . $survey->getId()->getValue());

        $this->transformer->transform($survey, $entity);

        $this->entityRepository->save($entity, true);
    }

    #[Override]
    public function find(string $surveyId): Survey
    {
        $entity = $this->entityRepository->find($surveyId) ?? throw new LogicException('Unable to find survey ' . $surveyId);

        return $this->transformer->transform($entity, Survey::class, ['SurveyDomain_SurveyEntity']);
    }

    #[\Override]
    public function findAll(): array
    {
        $entities = $this->entityRepository->findAll();

        return $this->transformer->transform($entities, Survey::class . '[]', ['SurveyDomain_SurveyEntity']);
    }

    #[\Override]
    public function remove(Survey $survey): void
    {
        $entity = $this->entityRepository->find($survey->getId()->getValue()) ?? throw new LogicException('Unable to delete survey, not found ' . $survey->getId()->getValue());

        $this->entityRepository->remove($entity, true);
    }
}
