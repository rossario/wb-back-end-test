<?php

namespace App\Infrastructure\Persistence\Repository;

use App\Components\Survey\Domain\Entity\Answer;
use App\Infrastructure\Persistence\Doctrine\Entity\Answer as AnswerEntity;
use App\Infrastructure\Persistence\Doctrine\Repository\AnswerEntityRepository;
use App\Infrastructure\Persistence\Service\Transformer;
use App\Port\Secondary\Persistance\Repository\Survey\AnswerRepository as AnswerRepositoryInterface;
use Override;

class AnswerRepository implements AnswerRepositoryInterface
{
    public function __construct(
        private AnswerEntityRepository $entityRepository,
        private Transformer $transformer,
    ) {
    }

    #[Override]
    public function create(Answer $answer): void
    {
        $entity = $this->transformer->transform($answer, AnswerEntity::class, ['AnswerModel_AnswerEntity']);

        $this->entityRepository->save($entity, true);
    }
}
