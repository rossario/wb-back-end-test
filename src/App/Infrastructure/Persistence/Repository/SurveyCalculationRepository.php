<?php

namespace App\Infrastructure\Persistence\Repository;

use App\Components\Survey\Domain\Repository\SurveyRepository;
use App\Infrastructure\Persistence\Doctrine\Repository\SurveyEntityRepository;
use App\SharedKernel\ValueObject\Concrete\SurveyId;

class SurveyCalculationRepository implements SurveyRepository
{
    public function __construct(
        private SurveyEntityRepository $entityRepository,
    )
    {}

    #[\Override]
    public function getNumberOfAnswers(SurveyId $id): int
    {
        // TODO: Implement getNumberOfAnswers() method.
        return 1;
    }

    #[\Override]
    public function getAverageSurveyQuality(SurveyId $id): int
    {
        // TODO: Implement getAverageSurveyQuality() method.
        return 2;
    }

    #[\Override]
    public function getAllSurveyCommentsList(SurveyId $id): array
    {
        // TODO: Implement getAllSurveyCommentsList() method.
        return [];
    }
}
