<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Service;

use App\SharedKernel\Port\Service\Transformer as TransformerInterface;
use Override;
use Symfony\Component\Serializer\Serializer;

class Transformer implements TransformerInterface
{
    public function __construct(
        private Serializer $serializer
    ) {
    }

    #[Override]
    public function transform(mixed $from, object|string $to = null, array $groups = []): mixed
    {
        if (is_string($to)) {
            return $this->create($from, $to, $groups);
        }

        return $this->update($from, $to, $groups);
    }

    public function create(mixed $from, string $to, array $groups): mixed
    {
        class_exists(str_replace('[]', '', $to))
        || (
            throw new \LogicException(
                interface_exists(str_replace('[]', '', $to))
                    ? sprintf('Can\'t create interface instance, has to be a class: %s', $to)
                    : sprintf('Invalid class: %s', $to)
            )
        );

        $data = $this->serializer->normalize($from, context: compact('groups'));

        return $this->serializer->denormalize($data, $to, context: compact('groups'));
    }

    public function update(mixed $from, object $to, array $groups): mixed
    {
        $data = $this->serializer->normalize($from, context: compact('groups'));

        return $this->serializer->denormalize($data, $to::class, context: ['object_to_populate' => $to, 'groups' => $groups]);
    }
}