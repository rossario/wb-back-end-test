<?php

declare(strict_types=1);

namespace App\Infrastructure\Notification\Service;

use App\SharedKernel\DTO\Report\ReportDTO;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

final readonly class ReportMailer
{
    public function __construct(
        private MailerInterface $mailer
    )
    {}

    public function send(ReportDTO $report): void
    {
        $email = (new Email())
            ->from('hello@example.com')
            ->to($report->survey->reportEmail)
            ->subject(sprintf('Report for survey "%s" is here!', $report->survey->name))
            ->text(
                sprintf(
                    'There should be a link to generated report but just id is also fine ;) - "%s"',
                    $report->id,
                ),
            );

        $this->mailer->send($email);
    }
}
