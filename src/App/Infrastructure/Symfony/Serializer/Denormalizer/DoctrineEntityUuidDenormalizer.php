<?php

namespace App\Infrastructure\Symfony\Serializer\Denormalizer;

use App\Infrastructure\Persistence\Doctrine\Interface\DoctrineEntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class DoctrineEntityUuidDenormalizer implements DenormalizerInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    )
    {}

    #[Override]
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->entityManager->find($type, $data);
    }

    #[Override]
    public function supportsDenormalization($data, string $type, string $format = null)
    {
        $result = is_string($data)
            && str_contains($type, '\\')
            && class_exists(str_replace('[]', '', $type))
            && in_array(DoctrineEntityInterface::class, class_implements(str_replace('[]', '', $type)))
            && Uuid::isValid($data);
        //throw new Exception();
        //var_dump('Supports: ' . $type, $data, $result, str_contains($type, '\\'), class_exists(str_replace('[]', '', $type)), in_array(DoctrineEntityInterface::class, class_implements(str_replace('[]', '', $type))));
        return $result;
    }
}