<?php

namespace App\Infrastructure\Symfony\Serializer\Denormalizer;

use Override;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class UuidDenormalizer implements DenormalizerInterface
{
    #[Override]
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return Uuid::fromString($data);
    }

    #[Override]
    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return $type === UuidInterface::class;
    }
}