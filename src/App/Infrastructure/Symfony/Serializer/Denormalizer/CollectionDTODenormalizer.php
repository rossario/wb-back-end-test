<?php

namespace App\Infrastructure\Symfony\Serializer\Denormalizer;

use App\UserInterface\Api\DTO\CollectionDTO;
use Override;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class CollectionDTODenormalizer implements DenormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    private const DUPLICATE_PREVENT = 'CollectionDTODenormalizerAlreadyProcessed';

    #[Override]
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->serializer->denormalize([
            'array' => $this->serializer->denormalize($data, $type::getSupportedInstanceType() . '[]', $format, $context + [self::DUPLICATE_PREVENT => true])
        ], $type, $format, $context + [self::DUPLICATE_PREVENT => true]);
    }

    #[Override]
    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return str_contains($type, '\\')
            && class_exists($type)
            && in_array(CollectionDTO::class, class_implements($type))
            && !isset($context[self::DUPLICATE_PREVENT]);
    }
}