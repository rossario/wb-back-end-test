<?php

namespace App\Infrastructure\Symfony\Serializer\Denormalizer;

use App\Infrastructure\Persistence\Doctrine\Interface\DoctrineEntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Override;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class DoctrineEntityDenormalizer implements DenormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    private const DUPLICATE_PREVENT = 'DoctrineEntityDenormalizerAlreadyProcessed';

    public function __construct(
        private EntityManagerInterface $entityManager,
    )
    {}

    #[Override]
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $entity = $this->entityManager->find($type, $data['id']);

        if ($entity) {
            $context['object_to_populate'] = $this->entityManager->find($type, $data['id']);
        }

        $context[self::DUPLICATE_PREVENT] = true;

        return $this->serializer->denormalize($data, $type, $format, $context);
    }

    #[Override]
    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        $result = !isset($context[self::DUPLICATE_PREVENT])
            && is_array($data)
            && isset($data['id'])
            && str_contains($type, '\\')
            && class_exists(str_replace('[]', '', $type))
            && in_array(DoctrineEntityInterface::class, class_implements(str_replace('[]', '', $type)));
        //throw new Exception();
        //var_dump('Supports: ' . $type, $data, $result, str_contains($type, '\\'), class_exists(str_replace('[]', '', $type)), in_array(DoctrineEntityInterface::class, class_implements(str_replace('[]', '', $type))));
        return $result;
    }
}