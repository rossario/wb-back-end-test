<?php

namespace App\Infrastructure\Symfony\Serializer\Denormalizer;

use App\SharedKernel\ValueObject\Interface\ReadableValue;
use Override;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class ReadableValueDenormalizer implements DenormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    private const DUPLICATE_PREVENT = 'ReadableValueDenormalizerAlreadyProcessed';

    #[Override]
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->serializer->denormalize(['value' => $data], $type, $format, [self::DUPLICATE_PREVENT => true]);
    }

    #[Override]
    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return !isset($context[self::DUPLICATE_PREVENT])
            && str_contains($type, '\\')
            && class_exists($type)
            && in_array(ReadableValue::class, class_implements($type));
    }
}