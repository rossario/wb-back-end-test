<?php

namespace App\Infrastructure\Symfony\Serializer\Normalizer;

use App\SharedKernel\ValueObject\Interface\ReadableValue;
use InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class ReadableValueNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function normalize($object, $format = null, array $context = [])
    {
        if (!$object instanceof ReadableValue) {
            throw new InvalidArgumentException();
        }

        return $this->serializer->normalize($object->getValue());
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ReadableValue;
    }
}