<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\EventListener;

use App\UserInterface\Api\DTO\OutputRootDTO;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class DTOResponseListener
{
    public function __construct(
        private SerializerInterface $serializer
    )
    {
    }

    #[AsEventListener(KernelEvents::VIEW)]
    public function onKernelView(ViewEvent $event): void
    {
        $result = $event->getControllerResult();

        if ($result instanceof OutputRootDTO) {
            $jsonOutput = $this->serializer->serialize($result, 'json');

            $event->setResponse(new JsonResponse($jsonOutput, json: true));
        }
    }
}