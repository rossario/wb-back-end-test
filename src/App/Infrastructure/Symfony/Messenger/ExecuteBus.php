<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Messenger;

use App\SharedKernel\Port\Service\ExecuteBus as ExecuteBusInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class ExecuteBus implements ExecuteBusInterface
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $messageBus
    )
    {
    }

    #[\Override]
    public function send(object $message): mixed
    {
        return $this->handle($message);
    }
}