<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Messenger;

use App\SharedKernel\Port\Service\QueryBus as QueryBusPort;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class QueryBus implements QueryBusPort
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $messageBus
    )
    {
    }

    #[\Override]
    public function get(object $query): mixed
    {
        return $this->handle($query);
    }
}