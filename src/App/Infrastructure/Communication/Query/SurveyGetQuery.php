<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Query;

use App\Port\Primary\Survey\Query\SurveyGet;
use Override;

final readonly class SurveyGetQuery implements SurveyGet
{
    public function __construct(
        private string $surveyId
    )
    {}

    #[Override]
    public function getSurveyId(): string
    {
        return $this->surveyId;
    }
}