<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Query;

use App\Port\Primary\Survey\Query\SurveySearch;

final readonly class SurveySearchQuery implements SurveySearch
{}