<?php

namespace App\Infrastructure\Communication\Query;

class ReportViewQuery
{
    public function __construct(
        public string $reportId
    )
    {}
}