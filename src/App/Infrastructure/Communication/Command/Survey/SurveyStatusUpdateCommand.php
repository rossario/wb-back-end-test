<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Command\Survey;

use App\Port\Primary\Survey\Command\SurveyStatusUpdate;

final readonly class SurveyStatusUpdateCommand implements SurveyStatusUpdate
{
    public function __construct(
        public string $surveyId,
        public string $status,
    )
    {}

    public function getSurveyId(): string
    {
        return $this->surveyId;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}