<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Command\Survey;

use App\Port\Primary\Survey\Command\SurveyCreate;

final readonly class SurveyCreateCommand implements SurveyCreate
{
    public function __construct(
        public string $name,
        public string $reportEmail,
    )
    {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getReportEmail(): string
    {
        return $this->reportEmail;
    }
}