<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Command\Survey;

use App\Port\Primary\Survey\Command\SurveyRemove;

final readonly class SurveyRemoveCommand implements SurveyRemove
{
    public function __construct(
        public string $surveyId,
    )
    {}

    public function getSurveyId(): string
    {
        return $this->surveyId;
    }
}