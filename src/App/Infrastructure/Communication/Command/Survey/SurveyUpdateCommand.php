<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Command\Survey;

use App\Port\Primary\Survey\Command\SurveyUpdate;

final readonly class SurveyUpdateCommand implements SurveyUpdate
{
    public function __construct(
        public string $surveyId,
        public string $name,
        public string $email,
    )
    {}

    public function getSurveyId(): string
    {
        return $this->surveyId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}