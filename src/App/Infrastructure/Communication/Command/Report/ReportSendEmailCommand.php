<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Command\Report;

use App\Port\Primary\Report\Command\ReportSendEmail;

final readonly class ReportSendEmailCommand implements ReportSendEmail
{
    public function __construct(
        public string $reportId,
    )
    {}

    public function getReportId(): string
    {
        return $this->reportId;
    }
}