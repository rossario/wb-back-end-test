<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Command\Report;

use App\Port\Primary\Report\Command\ReportCreate;

final readonly class ReportCreateCommand implements ReportCreate
{
    public function __construct(
        public string $survey,
    )
    {}

    public function getSurvey(): string
    {
        return $this->survey;
    }
}