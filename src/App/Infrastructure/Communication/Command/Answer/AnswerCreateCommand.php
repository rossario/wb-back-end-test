<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Command\Answer;

use App\Port\Primary\Survey\Command\AnswerCreate;

final readonly class AnswerCreateCommand implements AnswerCreate
{
    public function __construct(
        public string $survey,
        public int $quality,
        public string $comment,
    )
    {}

    public function getSurvey(): string
    {
        return $this->survey;
    }

    public function getQuality(): int
    {
        return $this->quality;
    }

    public function getComment(): string
    {
        return $this->comment;
    }
}