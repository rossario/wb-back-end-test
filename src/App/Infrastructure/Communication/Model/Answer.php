<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Model;

use Symfony\Component\Serializer\Attribute\Groups;

readonly class Answer
{
    public function __construct(
        #[Groups(['AnswerModel_AnswerEntity'])]
        public string $id,

        #[Groups(['AnswerModel_AnswerEntity'])]
        public Survey $survey,

        #[Groups(['AnswerModel_AnswerEntity'])]
        public string $quality,

        #[Groups(['AnswerModel_AnswerEntity'])]
        public string $comment,
    )
    {
    }
}