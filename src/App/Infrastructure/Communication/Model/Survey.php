<?php

namespace App\Infrastructure\Communication\Model;

use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;

readonly class Survey
{
    public function __construct(
        public string $id,
        public string $name,
        public SurveyStatusEnum $status,
        public string $reportEmail,
    )
    {}
}