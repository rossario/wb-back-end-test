<?php

namespace App\Infrastructure\Communication\Model;

use DateTimeImmutable;

readonly class Report
{
    public function __construct(
        public string $id,
        public Survey $survey,
        public array $comments,
        public int $numberOfAnswers,
        public DateTimeImmutable $generatedAt,
    )
    {
    }
}