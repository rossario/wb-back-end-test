<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Service;

use App\Infrastructure\Communication\Command\Answer\AnswerCreateCommand;
use App\Infrastructure\Communication\Command\Survey\SurveyCreateCommand;
use App\Infrastructure\Communication\Command\Survey\SurveyRemoveCommand;
use App\Infrastructure\Communication\Command\Survey\SurveyStatusUpdateCommand;
use App\Infrastructure\Communication\Command\Survey\SurveyUpdateCommand;
use App\Infrastructure\Communication\Query\SurveyGetQuery;
use App\Infrastructure\Communication\Query\SurveySearchQuery;
use App\Infrastructure\Symfony\Messenger\ExecuteBus;
use App\Port\Primary\Survey\Response\SurveyList;
use App\Port\Secondary\Communication\Survey\AnswerCreator;
use App\Port\Secondary\Communication\Survey\SurveyCreator;
use App\Port\Secondary\Communication\Survey\SurveyFinder;
use App\Port\Secondary\Communication\Survey\SurveyRemover;
use App\Port\Secondary\Communication\Survey\SurveyStatusUpdater;
use App\Port\Secondary\Communication\Survey\SurveyUpdater;
use App\SharedKernel\DTO\Survey\SurveyDTO;
use App\SharedKernel\Port\Service\QueryBus;
use Override;

readonly class SurveyService implements AnswerCreator, SurveyCreator, SurveyUpdater, SurveyStatusUpdater, SurveyFinder, SurveyRemover
{
    public function __construct(
        private ExecuteBus $deliveryBus,
        private QueryBus $queryBus
    ) {}

    #[Override]
    public function updateSurveyStatus(string $surveyId, string $status): mixed
    {
        return $this->deliveryBus->send(new SurveyStatusUpdateCommand($surveyId, $status));
    }

    #[Override]
    public function updateSurvey(string $surveyId, string $name, string $email): mixed
    {
        return $this->deliveryBus->send(new SurveyUpdateCommand($surveyId, $name, $email));
    }

    #[Override]
    public function answerCreate(string $surveyId, int $quality, string $comment): mixed
    {
        return $this->deliveryBus->send(new AnswerCreateCommand($surveyId, $quality, $comment));
    }

    #[Override]
    public function surveyCreate(string $name, string $reportEmail): mixed
    {
        return $this->deliveryBus->send(new SurveyCreateCommand($name, $reportEmail));
    }

    #[\Override]
    public function removeSurvey(string $surveyId): mixed
    {
        return $this->deliveryBus->send(new SurveyRemoveCommand($surveyId));
    }

    #[Override]
    public function find(string $surveyId): SurveyDTO
    {
        return $this->queryBus->get(new SurveyGetQuery($surveyId));
    }

    #[Override]
    public function findAll(): SurveyList
    {
        return $this->queryBus->get(new SurveySearchQuery());
    }

}