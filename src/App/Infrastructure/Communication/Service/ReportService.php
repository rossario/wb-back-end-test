<?php

declare(strict_types=1);

namespace App\Infrastructure\Communication\Service;

use App\Infrastructure\Communication\Command\Report\ReportCreateCommand;
use App\Infrastructure\Communication\Command\Report\ReportSendEmailCommand;
use App\Infrastructure\Communication\Query\ReportViewQuery;
use App\Infrastructure\Symfony\Messenger\ExecuteBus;
use App\Port\Primary\Report\Response\Report;
use App\Port\Secondary\Communication\Report\ReportCreator;
use App\Port\Secondary\Communication\Report\ReportFinder;
use App\Port\Secondary\Communication\Report\ReportNotifier;
use App\SharedKernel\Port\Service\QueryBus;
use Override;

readonly class ReportService implements ReportFinder, ReportCreator, ReportNotifier
{
    public function __construct(
        private ExecuteBus $commandBus,
        private QueryBus $queryBus
    ) {}

    #[Override]
    public function create(string $surveyId): mixed
    {
        return $this->commandBus->send(new ReportCreateCommand($surveyId));
    }

    #[Override]
    public function notify(string $reportId): mixed
    {
        return $this->commandBus->send(new ReportSendEmailCommand($reportId));
    }

    #[Override]
    public function find(string $reportId): Report
    {
        return $this->queryBus->get(new ReportViewQuery($reportId));
    }
}