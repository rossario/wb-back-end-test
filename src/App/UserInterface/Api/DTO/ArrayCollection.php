<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO;

use ArrayIterator;
use LogicException;
use Override;

abstract class ArrayCollection extends ArrayIterator
{
    public function __construct(array $array = [])
    {
        $this->validateInputData($array);

        parent::__construct($array);
    }

    private function validateInputData(array $array): void
    {
        $supportedInstanceType = static::getSupportedInstanceType();
        in_array(DTO::class, class_implements($supportedInstanceType)) || throw new LogicException('Unsupported class ' . $supportedInstanceType);

        array_walk(
            $array,
            fn($value) => $value instanceof $supportedInstanceType || throw new LogicException()
        );
    }

    #[Override]
    public function offsetSet(mixed $key, mixed $value): never
    {
        throw new LogicException('This collection is immutable');
    }

    #[Override]
    public function offsetUnset(mixed $key): never
    {
        throw new LogicException('This collection is immutable');
    }
}