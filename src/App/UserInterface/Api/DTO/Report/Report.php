<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Report;

use App\UserInterface\Api\DTO\OutputRootDTO;

readonly class Report implements OutputRootDTO
{
    public function __construct(
        public string $id,
        public Survey $survey,
        public int $quality,
        public int $numberOfAnswers,
        public array $comments,
        public \DateTimeImmutable $generatedAt,
    )
    {}
}