<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Report;

use App\UserInterface\Api\DTO\OutputDTO;

readonly class Survey implements OutputDTO
{
    public function __construct(
        public string $id,
        public string $name,
        public string $status,
    )
    {}
}