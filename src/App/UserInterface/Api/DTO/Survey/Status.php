<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Survey;

use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;
use App\UserInterface\Api\DTO\InputRootDTO;
use Symfony\Component\Validator\Constraints as Assert;

readonly class Status implements InputRootDTO
{
    public function __construct(
        #[Assert\Choice(callback: SurveyStatusEnum::class .'::validValues')]
        public string $status,
    )
    {}
}