<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Survey;

use App\UserInterface\Api\DTO\OutputDTO;

readonly class Answer implements OutputDTO
{
    public function __construct(
        public string $service,
        public int $quality,
        public ?string $comment,
    )
    {}
}