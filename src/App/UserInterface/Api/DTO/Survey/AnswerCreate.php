<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Survey;

use App\UserInterface\Api\DTO\InputRootDTO;

readonly class AnswerCreate implements InputRootDTO
{
    public function __construct(
        public string $survey,
        public int $quality,
        public ?string $comment,
    )
    {}
}