<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Survey;

use App\UserInterface\Api\DTO\InputRootDTO;

readonly class SurveyData implements InputRootDTO
{
    public function __construct(
        public string $name,
        public string $reportEmail,
    )
    {}
}