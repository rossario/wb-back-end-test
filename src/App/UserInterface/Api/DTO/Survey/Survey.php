<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Survey;

use App\UserInterface\Api\DTO\OutputRootDTO;

readonly class Survey implements OutputRootDTO
{
    public function __construct(
        public string $id,
        public string $name,
        public string $status,
        public string $reportEmail,
        public AnswersCollection $answers,
    )
    {}
}