<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Survey;

use App\UserInterface\Api\DTO\ArrayCollection;
use App\UserInterface\Api\DTO\CollectionDTO;
use App\UserInterface\Api\DTO\OutputDTO;

class AnswersCollection extends ArrayCollection implements OutputDTO, CollectionDTO
{
    #[\Override]
    public static function getSupportedInstanceType(): string
    {
        return Answer::class;
    }
}