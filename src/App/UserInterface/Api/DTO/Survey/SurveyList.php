<?php

declare(strict_types=1);

namespace App\UserInterface\Api\DTO\Survey;

use App\UserInterface\Api\DTO\OutputRootDTO;

readonly class SurveyList implements OutputRootDTO
{
    public function __construct(
        public SurveyShortCollection $results,
    )
    {}
}