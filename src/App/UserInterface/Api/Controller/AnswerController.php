<?php

declare(strict_types=1);

namespace App\UserInterface\Api\Controller;

use App\Port\Secondary\Communication\Survey\AnswerCreator;
use App\Port\Secondary\Communication\Survey\SurveyFinder;
use App\SharedKernel\Port\Service\Transformer;
use App\UserInterface\Api\DTO\Survey\Answer;
use App\UserInterface\Api\DTO\Survey\Survey;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

final class AnswerController extends AbstractController
{
    #[Route('/survey/{id}/answer', methods: 'POST')]
    public function create(
        #[MapRequestPayload]
        Answer $answer,
        AnswerCreator $service,
        SurveyFinder $finder,
        Transformer $transformer
    ): Survey
    {
        $service->answerCreate($answer->service, $answer->quality, $answer->comment);

        return $transformer->transform($finder->find($answer->service), Survey::class);
    }
}
