<?php

declare(strict_types=1);

namespace App\UserInterface\Api\Controller;

use App\Port\Secondary\Communication\Survey\SurveyCreator;
use App\Port\Secondary\Communication\Survey\SurveyFinder;
use App\Port\Secondary\Communication\Survey\SurveyRemover;
use App\Port\Secondary\Communication\Survey\SurveyStatusUpdater;
use App\Port\Secondary\Communication\Survey\SurveyUpdater;
use App\SharedKernel\Port\Service\Transformer;
use App\UserInterface\Api\DTO\Survey\Status;
use App\UserInterface\Api\DTO\Survey\Survey;
use App\UserInterface\Api\DTO\Survey\SurveyData;
use App\UserInterface\Api\DTO\Survey\SurveyList;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/survey')]
class SurveyController extends AbstractController
{
    public function __construct(
        private readonly Transformer $transformer,
        private readonly SurveyFinder $finder,
    ) {
    }

    #[Route(methods: 'GET')]
    public function index(): SurveyList
    {
        return $this->transformer->transform(['results' => $this->finder->findAll()], SurveyList::class);
    }

    #[Route(methods: 'POST')]
    public function create(
        #[MapRequestPayload]
        SurveyData $data,
        SurveyCreator $service,
    ): Survey
    {
        $surveyId = $service->surveyCreate($data->name, $data->reportEmail);

        return $this->getSurvey($surveyId);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function edit(
        string $id,
        #[MapRequestPayload]
        SurveyData $data,
        SurveyUpdater $service
    ): Survey
    {
        $service->updateSurvey($id, $data->name, $data->reportEmail);

        return $this->getSurvey($id);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(
        string $id,
        SurveyRemover $service
    ): JsonResponse
    {
        $service->removeSurvey($id);

        return $this->json([]);
    }

    #[Route('/{id}/status', methods: 'PUT')]
    public function changeStatus(
        string $id,
        #[MapRequestPayload]
        Status $status,
        SurveyStatusUpdater $service,
    ): Survey
    {
        $service->updateSurveyStatus($id, $status->status);

        return $this->getSurvey($id);
    }

    #[Route('/{id}/report', methods: 'GET')]
    public function show(string $id): Survey
    {
        return $this->getSurvey($id);
    }

    private function getSurvey(string $id): Survey
    {
        $survey = $this->finder->find($id) ?? throw $this->createNotFoundException();

        return $this->transformer->transform($survey, Survey::class);
    }
}
