<?php

declare(strict_types=1);

namespace App\UserInterface\Api\Controller;

use App\Port\Secondary\Communication\Report\ReportFinder;
use App\SharedKernel\Port\Service\Transformer;
use App\UserInterface\Api\DTO\Report\Report;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    #[Route('/answer/{id}/report', methods: 'GET')]
    public function show(string $id, ReportFinder $finder, Transformer $transformer): Report
    {
        $finder = $finder->find($id) ?? throw $this->createNotFoundException();

        return $transformer->transform($finder, Report::class);
    }
}
