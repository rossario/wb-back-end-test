<?php

declare(strict_types=1);

namespace App\Components\Report\Application\CommandHandler;

use App\Contract\Report\Command\ReportSendEmailHandlerInterface;
use App\Infrastructure\Notification\Service\ReportMailer;
use App\Port\Primary\Report\Command\ReportSendEmail;
use App\Port\Secondary\Communication\Report\ReportFinder;

final readonly class ReportSendEmailHandler implements ReportSendEmailHandlerInterface
{
    public function __construct(
        private ReportFinder $finder,
        private ReportMailer $reportMailer,
    )
    {}

    public function handle(ReportSendEmail $command): void
    {
        $report = $this->finder->find($command->reportId) ?? Throw new \LogicException();

        $this->reportMailer->send($report);
    }
}