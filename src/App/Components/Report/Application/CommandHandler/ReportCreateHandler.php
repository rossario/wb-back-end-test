<?php

declare(strict_types=1);

namespace App\Components\Report\Application\CommandHandler;

use App\Contract\Report\Command\ReportCreateHandlerInterface;
use App\Components\Report\Domain\Service\ReportGenerator;
use App\Port\Primary\Report\Command\ReportCreate;
use App\Port\Secondary\Persistance\Repository\Report\ReportRepository;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository;

final readonly class ReportCreateHandler implements ReportCreateHandlerInterface
{
    public function __construct(
        private SurveyRepository $surveyRepository,
        private ReportRepository $reportRepository,
        private ReportGenerator $reportGenerator,
    )
    {}

    public function handle(ReportCreate $command): void
    {
        $survey = $this->surveyRepository->find($command->getSurvey());

        $report = $this->reportGenerator->createForSurvey($survey);

        $this->reportRepository->create($report);
    }
}