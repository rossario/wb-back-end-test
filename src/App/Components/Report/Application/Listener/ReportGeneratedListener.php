<?php

declare(strict_types=1);

namespace App\Components\Report\Application\Listener;

use App\Port\Secondary\Communication\Report\ReportNotifier;
use App\SharedKernel\Component\Report\Event\ReportGenerated;

final readonly class ReportGeneratedListener
{
    public function __construct(
        private ReportNotifier $notifier,
    )
    {
    }

    public function handle(ReportGenerated $event)
    {
        $this->notifier->notify($event->reportId);
    }
}