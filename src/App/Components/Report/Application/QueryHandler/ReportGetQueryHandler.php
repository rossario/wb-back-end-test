<?php

declare(strict_types=1);

namespace App\Components\Report\Application\QueryHandler;

use App\Contract\Report\Query\ReportSendEmailHandlerInterface;
use App\Port\Primary\Report\Query\ReportGet;
use App\Port\Primary\Report\Response\Report;
use App\Port\Secondary\Persistance\Repository\Report\ReportRepository;

final readonly class ReportGetQueryHandler implements ReportSendEmailHandlerInterface
{
    public function __construct(
        private ReportRepository $reportRepository,
    )
    {}

    public function handle(ReportGet $query): Report
    {
        return $this->reportRepository->find($query->getReportId());
    }
}