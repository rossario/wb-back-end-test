<?php

declare(strict_types=1);

namespace App\Components\Report\Domain\Entity;

use App\SharedKernel\ValueObject\Abstract\Counter;
use App\SharedKernel\ValueObject\Abstract\DateValue;
use App\SharedKernel\ValueObject\Abstract\TextList;
use App\SharedKernel\ValueObject\Concrete\QualityScore;
use App\SharedKernel\ValueObject\Concrete\ReportId;
use App\SharedKernel\ValueObject\Concrete\SurveyId;

final readonly class Report
{
    public function __construct(
        private ReportId $id,
        private SurveyId $survey,
        private QualityScore $quality,
        private TextList $comments,
        private Counter $numberOfAnswers,
        private DateValue $generatedAt,
    )
    {
    }

    public function getId(): ReportId
    {
        return $this->id;
    }

    public function getSurvey(): SurveyId
    {
        return $this->survey;
    }

    public function getQuality(): QualityScore
    {
        return $this->quality;
    }

    public function getComments(): TextList
    {
        return $this->comments;
    }

    public function getNumberOfAnswers(): Counter
    {
        return $this->numberOfAnswers;
    }

    public function getGeneratedAt(): DateValue
    {
        return $this->generatedAt;
    }
}
