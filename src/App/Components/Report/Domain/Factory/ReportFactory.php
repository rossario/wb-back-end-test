<?php

declare(strict_types=1);

namespace App\Components\Report\Domain\Factory;

use App\Components\Report\Domain\Entity\Report;
use App\Components\Survey\Domain\Entity\Survey;
use App\SharedKernel\Domain\Exception\DomainException;
use App\SharedKernel\Port\Service\Timer;
use App\SharedKernel\Port\Service\UuidV6IdGenerator;
use App\SharedKernel\ValueObject\Abstract\Counter;
use App\SharedKernel\ValueObject\Abstract\DateValue;
use App\SharedKernel\ValueObject\Abstract\TextList;
use App\SharedKernel\ValueObject\Concrete\QualityScore;
use App\SharedKernel\ValueObject\Concrete\ReportId;
use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;

final readonly class ReportFactory
{
    public function __construct(
        private UuidV6IdGenerator $idGenerator,
        private Timer $timer
    )
    {}

    public function create(
        Survey $survey,
        QualityScore $answerQuality,
        Counter $numberOfAnswers,
        TextList $comments,
    ): Report
    {
        SurveyStatusEnum::Closed === $survey->getStatus()->getValue() || throw new DomainException('app.answer.survey.not_closed');

        return new Report(
            new ReportId($this->idGenerator->get()),
            $survey->getId(),
            $answerQuality,
            $comments,
            $numberOfAnswers,
            new DateValue($this->timer->nowImmutable())
        );
    }
}
