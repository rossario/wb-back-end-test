<?php

declare(strict_types=1);

namespace App\Components\Report\Domain\Service;

use App\Components\Survey\Domain\Entity\Survey;
use App\SharedKernel\Component\Survey\Event\SurveyClosed;
use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;
use Psr\EventDispatcher\EventDispatcherInterface;

final readonly class SurveyStatusUpdater
{
    public function __construct(
        private EventDispatcherInterface $eventDispatcher,
    )
    {}

    public function updateStatus(Survey $survey, SurveyStatusEnum $status): void
    {
        $survey->updateStatus($status);

        if ($status === SurveyStatusEnum::Closed) {
            $this->eventDispatcher->dispatch(new SurveyClosed($survey->getId()->getValue()));
        }
    }
}