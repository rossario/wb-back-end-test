<?php

declare(strict_types=1);

namespace App\Components\Report\Domain\Service;

use App\Components\Report\Domain\Entity\Report;
use App\Components\Report\Domain\Factory\ReportFactory;
use App\Components\Survey\Domain\Entity\Survey;
use App\Components\Survey\Domain\Repository\SurveyRepository;
use App\SharedKernel\ValueObject\Abstract\Counter;
use App\SharedKernel\ValueObject\Abstract\TextList;
use App\SharedKernel\ValueObject\Concrete\QualityScore;

final readonly class ReportGenerator
{
    public function __construct(
        private SurveyRepository $surveyRepository,
        private ReportFactory $reportFactory
    )
    {}

    public function createForSurvey(Survey $survey): Report
    {
        $quality = $this->surveyRepository->getAverageSurveyQuality($survey->getId());
        $comments = $this->surveyRepository->getAllSurveyCommentsList($survey->getId());
        $numberOfAnswers = $this->surveyRepository->getNumberOfAnswers($survey->getId());

        return $this->reportFactory->create($survey, new QualityScore($quality), new Counter($numberOfAnswers), new TextList($comments));
    }
}