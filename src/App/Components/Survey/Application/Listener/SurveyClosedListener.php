<?php

declare(strict_types=1);

namespace App\Components\Survey\Application\Listener;

use App\Port\Secondary\Communication\Report\ReportCreator;
use App\SharedKernel\Component\Survey\Event\SurveyClosed;

final readonly class SurveyClosedListener
{
    public function __construct(
        private ReportCreator $creator,
    )
    {}

    public function handle(SurveyClosed $event)
    {
        $this->creator->create($event->surveyId);
    }
}