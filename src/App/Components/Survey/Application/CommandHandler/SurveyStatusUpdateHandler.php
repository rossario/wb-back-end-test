<?php

declare(strict_types=1);

namespace App\Components\Survey\Application\CommandHandler;

use App\Contract\Survey\Command\SurveyStatusUpdateHandlerInterface;
use App\Port\Primary\Survey\Command\SurveyStatusUpdate;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository;
use App\SharedKernel\Port\Service\MessageHandler;
use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;

class SurveyStatusUpdateHandler implements MessageHandler, SurveyStatusUpdateHandlerInterface
{
    public function __construct(
        private SurveyRepository $surveyRepository,
    )
    {}

    public function __invoke(SurveyStatusUpdate $command): void
    {
        $survey = $this->surveyRepository->find($command->getSurveyId());

        $survey->updateStatus(SurveyStatusEnum::from($command->getStatus()));

        $this->surveyRepository->update($survey);
    }
}