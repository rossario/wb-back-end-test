<?php

declare(strict_types=1);

namespace App\Components\Survey\Application\CommandHandler;

use App\Port\Primary\Survey\Command\SurveyRemove;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository;
use App\SharedKernel\Port\Service\MessageHandler;

class SurveyRemoveHandler implements MessageHandler, \App\Contract\Survey\Command\SurveyRemoveHandler
{
    public function __construct(
        private SurveyRepository $surveyRepository,
    )
    {}

    public function __invoke(SurveyRemove $command): string
    {
        $survey = $this->surveyRepository->find($command->getSurveyId());

        $this->surveyRepository->remove($survey);

        return $survey->getId()->getValue();
    }
}