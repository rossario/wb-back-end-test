<?php

declare(strict_types=1);

namespace App\Components\Survey\Application\CommandHandler;

use App\Contract\Survey\Command\SurveyCreateHandlerInterface;
use App\Components\Survey\Domain\Factory\SurveyFactory;
use App\Components\Survey\Domain\ValueObject\SurveyName;
use App\Port\Primary\Survey\Command\SurveyCreate;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository;
use App\SharedKernel\Port\Service\MessageHandler;
use App\SharedKernel\ValueObject\Concrete\Email;

class SurveyCreateHandler implements MessageHandler, SurveyCreateHandlerInterface
{
    public function __construct(
        private SurveyRepository $surveyRepository,
        private SurveyFactory $surveyFactory
    )
    {}

    public function __invoke(SurveyCreate $command): string
    {
        $survey = $this->surveyFactory->create(
            new SurveyName($command->name),
            new Email($command->reportEmail)
        );

        $this->surveyRepository->create($survey);

        return $survey->getId()->getValue();
    }
}