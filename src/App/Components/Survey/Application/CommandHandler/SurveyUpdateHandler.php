<?php

declare(strict_types=1);

namespace App\Components\Survey\Application\CommandHandler;

use App\Contract\Survey\Command\SurveyUpdateHandlerInterface;
use App\Components\Survey\Domain\ValueObject\SurveyName;
use App\Port\Primary\Survey\Command\SurveyUpdate;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository;
use App\SharedKernel\Port\Service\MessageHandler;
use App\SharedKernel\ValueObject\Concrete\Email;

class SurveyUpdateHandler implements MessageHandler, SurveyUpdateHandlerInterface
{
    public function __construct(
        private SurveyRepository $surveyRepository
    )
    {}

    public function __invoke(SurveyUpdate $command): void
    {
        $survey = $this->surveyRepository->find($command->getSurveyId());

        $survey->updateMetadata(new SurveyName($command->getName()), new Email($command->getEmail()));

        $this->surveyRepository->update($survey);
    }
}