<?php

declare(strict_types=1);

namespace App\Components\Survey\Application\CommandHandler;

use App\Contract\Survey\Command\AnswerCreateHandlerInterface;
use App\Components\Survey\Domain\Factory\AnswerFactory;
use App\Components\Survey\Domain\ValueObject\AnswerComment;
use App\Port\Primary\Survey\Command\AnswerCreate;
use App\Port\Secondary\Persistance\Repository\Survey\AnswerRepository;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository;
use App\SharedKernel\Port\Service\MessageHandler;
use App\SharedKernel\Port\Service\Transformer;
use App\SharedKernel\ValueObject\Concrete\QualityScore;

final readonly class AnswerCreateHandler implements AnswerCreateHandlerInterface, MessageHandler
{
    public function __construct(
        private AnswerRepository $answerRepository,
        private SurveyRepository $surveyRepository,
        private AnswerFactory $answerFactory,
        private Transformer $transformer,
    )
    {}

    public function __invoke(AnswerCreate $command): void
    {
        $survey = $this->surveyRepository->find($command->survey) ?? throw new \Exception();

        $answer = $this->answerFactory->createForSurvey(
            $survey,
            new QualityScore($command->quality),
            new AnswerComment($command->comment)
        );

        $this->answerRepository->create($answer);
    }
}