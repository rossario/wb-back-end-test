<?php

declare(strict_types=1);

namespace App\Components\Survey\Application\QueryHandler;

use App\Contract\Survey\Query\SurveyGetQueryHandlerInterface;
use App\Port\Primary\Survey\Query\SurveyGet;
use App\Port\Primary\Survey\Response\Survey;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository;
use App\SharedKernel\DTO\Survey\SurveyDTO;
use App\SharedKernel\Port\Service\MessageHandler;
use App\SharedKernel\Port\Service\Transformer;

final readonly class SurveyGetQueryHandler implements MessageHandler, SurveyGetQueryHandlerInterface
{
    public function __construct(
        private SurveyRepository $surveyRepository,
        private Transformer $transformer,
    )
    {}

    public function __invoke(SurveyGet $query): Survey
    {
        return $this->transformer->transform($this->surveyRepository->find($query->getSurveyId()), SurveyDTO::class);
    }
}