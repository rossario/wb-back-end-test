<?php

declare(strict_types=1);

namespace App\Components\Survey\Application\QueryHandler;

use App\Contract\Survey\Query\SurveyListQueryHandlerInterface;
use App\Port\Primary\Survey\Query\SurveySearch;
use App\Port\Primary\Survey\Response\SurveyList as SurveyListResponse;
use App\Port\Secondary\Persistance\Repository\Survey\SurveyRepository;
use App\SharedKernel\Port\Service\MessageHandler;
use App\SharedKernel\Port\Service\Transformer;

final readonly class SurveyListQueryHandler implements MessageHandler, SurveyListQueryHandlerInterface
{
    public function __construct(
        private SurveyRepository $surveyRepository,
        private Transformer $transformer,
    ) {
    }

    public function __invoke(SurveySearch $query): SurveyListResponse
    {
        return $this->transformer->transform($this->surveyRepository->findAll(), SurveyListResponse::class);
    }
}