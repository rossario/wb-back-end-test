<?php

declare(strict_types=1);

namespace App\Components\Survey\Domain\Factory;

use App\Components\Survey\Domain\Entity\Answer;
use App\Components\Survey\Domain\Entity\Survey;
use App\Components\Survey\Domain\ValueObject\AnswerComment;
use App\SharedKernel\Domain\Exception\DomainException;
use App\SharedKernel\Port\Service\UuidV6IdGenerator;
use App\SharedKernel\ValueObject\Concrete\AnswerId;
use App\SharedKernel\ValueObject\Concrete\QualityScore;
use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;

final readonly class AnswerFactory
{
    public function __construct(
        private UuidV6IdGenerator $idGenerator,
    )
    {}

    public function createForSurvey(
        Survey $survey,
        QualityScore $quality,
        AnswerComment $comment = null,
    ): Answer
    {
        SurveyStatusEnum::Live === $survey->getStatus()->getValue() || throw new DomainException('app.answer.survey.not_live');

        return new Answer(
            new AnswerId($this->idGenerator->get()),
            $survey->getId(),
            $quality,
            $comment
        );
    }
}
