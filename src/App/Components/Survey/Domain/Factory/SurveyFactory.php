<?php

declare(strict_types=1);

namespace App\Components\Survey\Domain\Factory;

use App\Components\Survey\Domain\Entity\Survey;
use App\Components\Survey\Domain\ValueObject\SurveyName;
use App\Components\Survey\Domain\ValueObject\SurveyStatus;
use App\SharedKernel\Port\Service\UuidV6IdGenerator;
use App\SharedKernel\ValueObject\Concrete\Email;
use App\SharedKernel\ValueObject\Concrete\SurveyId;
use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;

final readonly class SurveyFactory
{
    public function __construct(
        private UuidV6IdGenerator $idGenerator,
    )
    {}

    public function create(
        SurveyName $name,
        Email $reportEmail,
    ): Survey
    {
        return new Survey(
            new SurveyId($this->idGenerator->get()),
            $name,
            new SurveyStatus(SurveyStatusEnum::Draft),
            $reportEmail,
        );
    }
}
