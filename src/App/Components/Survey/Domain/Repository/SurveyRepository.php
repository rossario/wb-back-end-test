<?php

declare(strict_types=1);

namespace App\Components\Survey\Domain\Repository;

use App\SharedKernel\ValueObject\Concrete\SurveyId;

interface SurveyRepository
{
    public function getNumberOfAnswers(SurveyId $id): int;

    public function getAverageSurveyQuality(SurveyId $id): int;

    public function getAllSurveyCommentsList(SurveyId $id): array;
}