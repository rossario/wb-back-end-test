<?php

declare(strict_types=1);

namespace App\Components\Survey\Domain\ValueObject;

use App\SharedKernel\ValueObject\Interface\ReadableValue;

final readonly class AnswerComment implements ReadableValue
{
    public function __construct(
        public ?string $value,
    ) {}

    public function getValue(): ?string
    {
        return $this->value;
    }
}