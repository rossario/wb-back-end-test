<?php

declare(strict_types=1);

namespace App\Components\Survey\Domain\ValueObject;

use App\SharedKernel\ValueObject\Exception\InvalidArgumentException;
use App\SharedKernel\ValueObject\Interface\ReadableValue;
use Override;

final readonly class SurveyName implements ReadableValue
{
    private const int MIN_LENGTH = 3;
    private const int MAX_LENGTH = 150;

    public function __construct(
        private string $value
    )
    {
        is_string($value) || throw new InvalidArgumentException('app.errors.value_objects.survey_name.invalid_type');
        '' !== $value     || throw new InvalidArgumentException('app.errors.value_objects.survey_name.empty');

        $valueLength = mb_strlen($value);

        $valueLength >= self::MIN_LENGTH || throw new InvalidArgumentException('app.errors.value_objects.survey_name.too_short');
        $valueLength <= self::MAX_LENGTH || throw new InvalidArgumentException('app.errors.value_objects.survey_name.too_long');
    }

    #[Override]
    public function getValue(): string
    {
        return $this->value;
    }
}