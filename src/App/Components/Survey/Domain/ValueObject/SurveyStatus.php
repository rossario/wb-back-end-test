<?php

declare(strict_types=1);

namespace App\Components\Survey\Domain\ValueObject;

use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;
use App\SharedKernel\ValueObject\Exception\InvalidArgumentException;
use App\SharedKernel\ValueObject\Interface\ReadableValue;
use Override;

final readonly class SurveyStatus implements ReadableValue
{
    public function __construct(
        private SurveyStatusEnum $value,
    )
    {
    }

    #[Override]
    public function getValue(): SurveyStatusEnum
    {
        return $this->value;
    }

    public function switchTo(SurveyStatusEnum $value): self
    {
        $this->value->isAllowedToChange($value) || throw new InvalidArgumentException('app.value_objects.switch.unavailable');

        return new self($value);
    }
}