<?php

declare(strict_types=1);

namespace App\Components\Survey\Domain\Entity;

use App\Components\Survey\Domain\ValueObject\AnswerComment;
use App\SharedKernel\ValueObject\Concrete\AnswerId;
use App\SharedKernel\ValueObject\Concrete\QualityScore;
use App\SharedKernel\ValueObject\Concrete\SurveyId;
use Symfony\Component\Serializer\Attribute\Groups;

final readonly class Answer
{
    public function __construct(
        #[Groups(['AnswerModel_AnswerEntity'])]
        private AnswerId $id,

        #[Groups(['AnswerModel_AnswerEntity'])]
        private SurveyId $survey,

        #[Groups(['AnswerModel_AnswerEntity'])]
        private QualityScore $quality,

        #[Groups(['AnswerModel_AnswerEntity'])]
        private AnswerComment $comment,
    )
    {
    }

    public function getId(): AnswerId
    {
        return $this->id;
    }

    public function getSurvey(): SurveyId
    {
        return $this->survey;
    }

    public function getQuality(): QualityScore
    {
        return $this->quality;
    }

    public function getComment(): AnswerComment
    {
        return $this->comment;
    }
}
