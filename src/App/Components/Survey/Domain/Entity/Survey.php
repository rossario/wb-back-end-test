<?php

declare(strict_types=1);

namespace App\Components\Survey\Domain\Entity;

use App\Components\Survey\Domain\ValueObject\SurveyName;
use App\Components\Survey\Domain\ValueObject\SurveyStatus;
use App\SharedKernel\ValueObject\Concrete\Email;
use App\SharedKernel\ValueObject\Concrete\SurveyId;
use App\SharedKernel\ValueObject\Enum\SurveyStatusEnum;
use Symfony\Component\Serializer\Attribute\Groups;

final class Survey
{
    public function __construct(
        #[Groups(['SurveyDomain_SurveyEntity'])]
        private readonly SurveyId $id,

        #[Groups(['SurveyDomain_SurveyEntity'])]
        private SurveyName $name,

        #[Groups(['SurveyDomain_SurveyEntity'])]
        private SurveyStatus $status,

        #[Groups(['SurveyDomain_SurveyEntity'])]
        private Email $reportEmail,
    ) {
    }

    public function updateStatus(SurveyStatusEnum $status): void
    {
        $this->status = $this->status->switchTo($status);
    }

    public function updateMetadata(SurveyName $name, Email $reportEmail): void
    {
        $this->name = $name;
        $this->reportEmail = $reportEmail;
    }

    public function getId(): SurveyId
    {
        return $this->id;
    }

    public function getName(): SurveyName
    {
        return $this->name;
    }

    public function getStatus(): SurveyStatus
    {
        return $this->status;
    }

    public function getReportEmail(): Email
    {
        return $this->reportEmail;
    }
}
