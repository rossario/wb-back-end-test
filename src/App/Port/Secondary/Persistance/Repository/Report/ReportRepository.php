<?php

namespace App\Port\Secondary\Persistance\Repository\Report;

use App\Components\Report\Domain\Entity\Report;

interface ReportRepository
{
    public function create(Report $report): void;

    public function find(string $reportId): Report;
}