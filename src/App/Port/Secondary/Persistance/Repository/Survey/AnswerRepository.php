<?php

declare(strict_types=1);

namespace App\Port\Secondary\Persistance\Repository\Survey;

use App\Components\Survey\Domain\Entity\Answer;

interface AnswerRepository
{
    public function create(Answer $answer): void;
}