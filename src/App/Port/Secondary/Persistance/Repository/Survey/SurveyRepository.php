<?php

declare(strict_types=1);

namespace App\Port\Secondary\Persistance\Repository\Survey;

use App\Components\Survey\Domain\Entity\Survey;

interface SurveyRepository
{
    public function create(Survey $survey): void;

    public function remove(Survey $survey): void;

    public function update(Survey $survey): void;

    public function find(string $surveyId): Survey;

    public function findAll(): array;
}