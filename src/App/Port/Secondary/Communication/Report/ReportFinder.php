<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Report;

use App\Port\Primary\Report\Response\Report;

interface ReportFinder
{
    public function find(string $reportId): ?Report;
}