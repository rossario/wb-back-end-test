<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Report;

interface ReportNotifier
{
    public function notify(string $reportId): mixed;
}