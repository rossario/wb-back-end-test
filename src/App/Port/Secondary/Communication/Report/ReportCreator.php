<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Report;

interface ReportCreator
{
    public function create(string $surveyId): mixed;
}