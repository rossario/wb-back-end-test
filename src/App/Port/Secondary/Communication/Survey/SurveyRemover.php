<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Survey;

interface SurveyRemover
{
    public function removeSurvey(string $surveyId): mixed;
}