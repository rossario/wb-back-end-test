<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Survey;

use App\Port\Primary\Survey\Response\Survey;
use App\Port\Primary\Survey\Response\SurveyList;

interface SurveyFinder
{
    public function find(string $surveyId): ?Survey;

    /** @return array<Survey> */
    public function findAll(): SurveyList;
}