<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Survey;

interface SurveyUpdater
{
    public function updateSurvey(string $surveyId, string $name, string $email): mixed;
}