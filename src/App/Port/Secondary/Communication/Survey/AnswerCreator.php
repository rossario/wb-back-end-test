<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Survey;

interface AnswerCreator
{
    public function answerCreate(string $surveyId, int $quality, string $comment): mixed;
}