<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Survey;

interface SurveyStatusUpdater
{
    public function updateSurveyStatus(string $surveyId, string $status): mixed;
}