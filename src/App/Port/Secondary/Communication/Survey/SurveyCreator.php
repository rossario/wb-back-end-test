<?php

declare(strict_types=1);

namespace App\Port\Secondary\Communication\Survey;

interface SurveyCreator
{
    public function surveyCreate(string $name, string $reportEmail): mixed;
}