<?php

declare(strict_types=1);

namespace App\Port\Primary\Report\Query;

interface ReportGet
{
    public function getReportId(): string;
}