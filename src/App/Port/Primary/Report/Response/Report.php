<?php

declare(strict_types=1);

namespace App\Port\Primary\Report\Response;

interface Report
{
    public function getId(): string;

    public function getSurvey(): ReportSurvey;

    public function getQuality(): int;

    public function getComments(): array;

    public function getNumberOfAnswers(): int;
}