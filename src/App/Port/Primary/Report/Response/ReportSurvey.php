<?php

declare(strict_types=1);

namespace App\Port\Primary\Report\Response;

interface ReportSurvey
{
    public function getId(): string;

    public function getName(): string;

    public function getReportEmail(): string;
}