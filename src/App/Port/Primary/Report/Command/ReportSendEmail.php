<?php

declare(strict_types=1);

namespace App\Port\Primary\Report\Command;

interface ReportSendEmail
{
    public function getReportId(): string;
}