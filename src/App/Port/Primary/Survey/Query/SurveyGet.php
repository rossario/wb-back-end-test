<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Query;

interface SurveyGet
{
    public function getSurveyId(): string;
}