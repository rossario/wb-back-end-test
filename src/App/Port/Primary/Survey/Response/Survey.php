<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Response;

interface Survey
{
    public function getId(): string;

    public function getName(): string;

    public function getStatus(): string;

    public function getReportEmail(): string;

    public function getAnswers(): array;
}