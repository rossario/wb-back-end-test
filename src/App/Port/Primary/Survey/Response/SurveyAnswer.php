<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Response;

interface SurveyAnswer
{
    public function getId(): string;

    public function getQuality(): string;

    public function getComments(): ?string;
}