<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Response;

use App\Port\Primary\Survey\Query\SurveyGet;
use App\SharedKernel\ValueObject\Abstract\Collection;

/**
 * Just because PHP doesn't support declarative collection return type
 * I would be glad to remove it and replace in code return types from SurveyList to array<SurveyList>
 */
readonly class SurveyList extends Collection
{
    #[\Override]
    public function getItemType(): string
    {
        return SurveyGet::class;
    }
}