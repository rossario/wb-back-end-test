<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Command;

interface SurveyCreate
{
    public function getName(): string;

    public function getReportEmail(): string;
}