<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Command;

interface SurveyRemove
{
    public function getSurveyId(): string;
}