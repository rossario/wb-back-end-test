<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Command;

interface AnswerCreate
{
    public function getSurvey(): string;

    public function getQuality(): int;

    public function getComment(): string;
}