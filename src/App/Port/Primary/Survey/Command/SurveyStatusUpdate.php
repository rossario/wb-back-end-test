<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Command;

interface SurveyStatusUpdate
{
    public function getSurveyId(): string;

    public function getStatus(): string;
}