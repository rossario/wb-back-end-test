<?php

declare(strict_types=1);

namespace App\Port\Primary\Survey\Command;

interface SurveyUpdate
{
    public function getSurveyId(): string;

    public function getName(): string;

    public function getEmail(): string;
}