<?php

class Ent
{
    private function __construct(public string $a, public string $b)
    {
    }

    public static function fromFactory(Fac $factory, string $a, string $b): static
    {
        $ent = $factory->facPub(fn()=> new Ent(...func_get_args()), $a, $b);
        return $ent;
    }
}

class Fac
{
    private function facPrv()
    {
    }

    /**
     * @param Fac::fromFactory $constructor
     */
    public function facPub(callable $constructor, string $a, string $b) {
        return $constructor($a, $b);
    }
}

$factory = new Fac();
$ent = Ent::fromFactory($factory, 'a', 'b');

$a = 1;